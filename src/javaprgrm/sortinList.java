package javaprgrm;

import java.util.Arrays;

public class sortinList {

	public static void main(String[] args) {

		//Inbuilt method.
//		int [] a = {1,2,3,4,9,2};
//		Arrays.sort(a);
//		for (int i=0; i<a.length; i++) {
//			System.out.println(a[i]);
//		}
		
		int [] a = {1,2,3,4,9,2};
		int temp;
		int count=0;
		for (int i=0; i<a.length; i++) {
			temp=i;
			for(int j=i+1; j<a.length; j++) {
				if (a[j]< a[temp]) {
					temp = j;
				}
			}
			
			count = a[i];
			a[i] = a[temp];
			a[temp] = count;
		}
		for (int i=0; i<a.length; i++) {
			System.out.println(a[i]+ "");
		}
	}

}
