package javaprgrm;

import java.util.HashMap;

public class nonrepeatingcharacterinstring {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "hhello";
		int len = str.length();
		Character ch;
		HashMap<Character, Integer> h = new HashMap<>();
		for(int i=0; i<len; i++) {
			ch = str.charAt(i);
			if (h.containsKey(ch)) {
				h.put(ch, h.get(ch)+1);
			}
			else {
				h.put(ch, 1);
			}
		}
		
		for (int j=0; j<len; j++) {
			ch = str.charAt(j);
			if(h.get(ch)==1) {
				System.out.println(ch);
				break;
			}
		}
		

	}

}
