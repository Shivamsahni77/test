package javaprgrm;

import java.util.HashMap;
import java.util.Set;

public class countcharString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "Java";
		char [] ch = str.toCharArray();
		HashMap<Character, Integer> h = new HashMap<>();
		for (int i=0; i<ch.length; i++) {
			if (h.containsKey(ch[i])) {
				h.put(ch[i], h.get(ch[i])+1);
			}
			else {
				h.put(ch[i], 1);
			}
		}
		Set<Character> key = h.keySet();
		for (Character c : key) {
			if(h.get(c)>1) {
				System.out.println(c+"="+ h.get(c));
			}
		}

	}

}
